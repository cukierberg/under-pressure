# underPressure
underPressure to narzędzie umożliwiające osobom chorym na cukrzycę śledzenie swojego stanu zdrowia w łatwy sposób.
Stworzone jako aplikacja webowa jest dostępne z każdego miejsca na świecie, gdzie tylko jest dostęp do Internetu, bez konieczności instalacji.

# Autorzy
Projekt został stworzony w ramach zajęć _Języki Programowania do Zastosowań Biomedycznych w semestrze zimowym 2018/2019._
Autorzy projektu:
 * Alina Babiracka, 208546
 * Julia Pelc, 221380
 * Mateusz Skrzypecki, 221376

## Co oferuje underPressure
underPressure pozwala na śledzenie historii swoich pomiarów w kategoriach:
 * ciśnienie skurczowe/rozkurczowe
 * puls
 * poziom cukru
 * waga (+BMI)

## Technologie użyte w underPressure
underPressure zbudowane jest w oparciu o [Pythona](https://www.python.org/)  w wersji 3.7 oraz [Django](https://www.djangoproject.com/) w wersji 2.0.6.
Po stronie front-endowej (interfejs użytkownika) underPressure wykorzystano framework [Bootstrap v4](https://getbootstrap.com/) oraz bibliotekę [chartJS](https://www.chartjs.org/) w wersji 2.0.

## Instalacja środowiska
Dopóki underPressure jest w fazie rozwoju, w celu skorzystania z narzędzia należy zainstalować środowisko deweloperskie.

Pierwszym krokiem jest sklonowanie repozytorium z Bitbucketa do wybranego miejsca na dysku:
```sh
git clone https://babiracka@bitbucket.org/cukierberg/under-pressure.git
```
Następnie, w tym samym miejscu należy uruchomić środowisko wirtualne.
```sh
python3 -m venv myvenv
source myvenv/bin/activate
```
Kolejnym krokiem jest instalacja managera pakietów Pythona `pip` oraz `django` w uruchomionym środowisku.
```sh
python3 -m pip install --upgrade pip
python3 -m pip install django
```
Gdy wszystko zostało poprawnie zainstalowane, pozostało wykonanie migracji oraz uruchomienie serwera django.
```sh
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```
Jeśli nie wystąpiły żadne błędy, projekt można już uruchomić pod adresem lokalnym `127.0.0.1:8000`.

## Instrukcja użycia
Po otwarciu adresu użytkownikowi pokazuje się strona umożliwiająca zalogowanie oraz rejestrację (konieczną do korzystania z underPressure).
Po zalogowaniu użytkownik trafia na stronę domową. W pasku nawigacji znajdują się odnośniki:
* Dodaj pomiar (dodawanie nowych pomiarów)
* Pomiary (wykresy już dodanych pomiarów):
    * ciśnienie
    * puls
    * poziom cukru
    * waga
* [nazwa użytkownika] będąca odnośnikiem do widoku profilu
* wyloguj

Aby zobaczyć wykresy pomiarów, należy mieć dodany co najmniej jeden pomiar z wybranej kategorii.
W celu dodania pomiaru należy wejść w zakładkę "Dodaj pomiar" i wypełnić formularz.
Wykresy wagi są dostępne po uzupełnieniu wzrostu w profilu (koniecznego do wyliczenia BMI).

from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from django.views.generic.base import TemplateView
from accounts import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
    path('pomiary/', views.pomiary, name='pomiary'),
    path('pomiary/cisnienie', views.pomiary_cisnienie, name='pomiary'),
    path('pomiary/puls', views.pomiary_puls, name='pomiary'),
    path('pomiary/cukier', views.pomiary_cukier, name='pomiary'),
    path('pomiary/waga', views.pomiary_waga, name='pomiary'),
    path('pomiary/nowy', views.nowy_pomiar, name='nowy-pomiar'),
    path('profile/', views.profile, name='profile'),
    path('profile/edit/', views.profile_edit, name='profile_edit'),
    path('profile/password/', views.change_password, name='change_password'),
    path('about/', views.about, name='about'),
    url('pomiary/data/puls/', views.get_data_puls),
    url('pomiary/data/waga/', views.get_data_waga),
    url('pomiary/data/cukier/', views.get_data_cukier),
    url('pomiary/data/cisnienie/', views.get_data_ciesnienie),
]

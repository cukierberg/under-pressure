from django import forms
from django.forms import ModelForm
# from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from datetime import datetime
from .models import ProfileInfo


class PomiarCisnieniaForm(forms.Form):
    skurczowe = forms.IntegerField(label='skurczowe', initial=0, required=False)
    rozkurczowe = forms.IntegerField(label='rozkurczowe', initial=0, required=False)
    data_cis = forms.DateTimeField(label='data_cis', initial=datetime.now, required=False)
    puls = forms.IntegerField(label='puls', initial=0, required=False)
    data_puls = forms.DateTimeField(label='data_puls', initial=datetime.now, required=False)
    cukier = forms.IntegerField(label='cukier', initial=0, required=False)
    data_cukier = forms.DateTimeField(label='data_cukier', initial=datetime.now, required=False)
    waga = forms.IntegerField(label='waga', initial=0, required=False)
    data_waga = forms.DateTimeField(label='data_waga', initial=datetime.now, required=False)


class EditProfileForm(forms.Form):
    email = forms.EmailField(label='Adres e-mail', required=False)
    first_name = forms.CharField(label='Imię', required=False)
    last_name = forms.CharField(label='Nazwisko', required=False)
    wiek = forms.IntegerField(label='Wiek', required=False)
    wzrost = forms.IntegerField(label='Wzrost', required=False)


class ProfileInfoForm(ModelForm):
    class Meta:
        model = ProfileInfo
        fields = ['wiek', 'wzrost']


class UserCreateForm(UserCreationForm):
    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        if commit:
            profile = ProfileInfo.object.create({'uzytkownik': User})
            user.save()
            profile.save()
        return user

from django.contrib import admin
from .models import (
    ProfileInfo,
    Pomiar)


@admin.register(ProfileInfo)
class ProfileInfoAdmin(admin.ModelAdmin):
    list_display = ('uzytkownik', 'wzrost', 'wiek')


@admin.register(Pomiar)
class ProfileInfoAdmin(admin.ModelAdmin):
    list_display = ('uzytkownik', 'data_pomiaru', 'wartosc', 'kod_pomiaru')
    # # dodanie możliwości filtrowania
    #   list_filter = ('skurczowe', 'rozkurczowe')
    # # dodanie szukania
    #   search_fields = ('skurczowe', 'rozkurczowe')

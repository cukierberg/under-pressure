from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
    

class Pomiar(models.Model):
    wartosc = models.IntegerField()
    kod_pomiaru = models.CharField(max_length=6, default='pomiar', blank=True)
    uzytkownik = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    data_pomiaru = models.DateTimeField(default=datetime.now, editable=True)


class ProfileInfo(models.Model):
    wzrost = models.IntegerField(blank=True, null=True)
    wiek = models.IntegerField(blank=True, null=True)
    uzytkownik = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

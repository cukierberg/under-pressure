from django.urls import reverse_lazy
from django.http import JsonResponse
from django.views import generic
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import User
from .models import (
    ProfileInfo,
    Pomiar)
from .forms import (
    EditProfileForm,
    PomiarCisnieniaForm)


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


def profile(request):
    user = request.user
    if not ProfileInfo.objects.filter(uzytkownik=user).exists():
        profil = ProfileInfo()
        profil.uzytkownik = user
        profil.save()
    info = ProfileInfo.objects.get(uzytkownik=user)
    args = {'user': user, 'info': info}
    return render(request, 'profile.html', args)


def pomiary(request):
    return render(request, 'pomiary.html')


def pomiary_cisnienie(request):
    lista_pomiarow = Pomiar.objects.filter(uzytkownik=request.user,
                                           kod_pomiaru__contains='cis').order_by("-data_pomiaru")
    args = {'lista': lista_pomiarow}
    return render(request, 'pomiary_cisnienie.html', args)


def pomiary_puls(request):
    lista_pomiarow = Pomiar.objects.filter(uzytkownik=request.user, kod_pomiaru='puls').order_by("-data_pomiaru")
    args = {'lista': lista_pomiarow}
    return render(request, 'pomiary_puls.html', args)


def get_data_puls(request, *args, **kwargs):
        lista_pomiarow = Pomiar.objects.filter(uzytkownik=request.user, kod_pomiaru='puls').order_by(
            "data_pomiaru")
        data_pomiarow =[]
        wyniki_pomiarow = []
        for pomiar in lista_pomiarow:
            data_pomiarow.append(pomiar.data_pomiaru)
            wyniki_pomiarow.append(pomiar.wartosc)

        data = {
            "wyniki": wyniki_pomiarow,
            "data_pomiaru": data_pomiarow,
        }
        return JsonResponse(data)


def get_data_cukier(request, *args, **kwargs):
    lista_pomiarow = Pomiar.objects.filter(uzytkownik=request.user, kod_pomiaru='cukier').order_by(
        "data_pomiaru")
    data_pomiarow = []
    wyniki_pomiarow = []
    for pomiar in lista_pomiarow:
        data_pomiarow.append(pomiar.data_pomiaru)
        wyniki_pomiarow.append(pomiar.wartosc)

    data = {
        "wyniki": wyniki_pomiarow,
        "data_pomiaru": data_pomiarow,
    }
    return JsonResponse(data)

def get_data_waga(request, *args, **kwargs):
    lista_pomiarow = Pomiar.objects.filter(uzytkownik=request.user, kod_pomiaru='waga').order_by(
        "data_pomiaru")
    data_pomiarow = []
    wyniki_pomiarow = []
    for pomiar in lista_pomiarow:
        data_pomiarow.append(pomiar.data_pomiaru)
        wyniki_pomiarow.append(pomiar.wartosc)

    data = {
        "wyniki": wyniki_pomiarow,
        "data_pomiaru": data_pomiarow,
    }
    return JsonResponse(data)


def get_data_ciesnienie(request, *args, **kwargs):
    lista_pomiarow_sku = Pomiar.objects.filter(uzytkownik=request.user, kod_pomiaru='cissku').order_by(
        "data_pomiaru")
    data_pomiarow_sku = []
    wyniki_pomiarow_sku = []
    for pomiar in lista_pomiarow_sku:
        data_pomiarow_sku.append(pomiar.data_pomiaru)
        wyniki_pomiarow_sku.append(pomiar.wartosc)

    lista_pomiarow_roz = Pomiar.objects.filter(uzytkownik=request.user, kod_pomiaru='cisroz').order_by(
        "data_pomiaru")
    data_pomiarow_roz = []
    wyniki_pomiarow_roz = []
    for pomiar in lista_pomiarow_roz:
        data_pomiarow_roz.append(pomiar.data_pomiaru)
        wyniki_pomiarow_roz.append(pomiar.wartosc)

    data_pomiarow_sku = data_pomiarow_sku + data_pomiarow_roz
    data = {
        "wyniki_sku": wyniki_pomiarow_sku,
        "data_pomiaru_sku": data_pomiarow_sku,
        "wyniki_roz": wyniki_pomiarow_roz,
        "data_pomiaru_roz": data_pomiarow_roz,
    }
    return JsonResponse(data)


def pomiary_cukier(request):
    lista_pomiarow = Pomiar.objects.filter(uzytkownik=request.user, kod_pomiaru='cukier').order_by("-data_pomiaru")
    args = {'lista': lista_pomiarow}
    return render(request, 'pomiary_cukier.html', args)


def pomiary_waga(request):
    lista_pomiarow = Pomiar.objects.filter(uzytkownik=request.user, kod_pomiaru='waga').order_by("-data_pomiaru")
    info = ProfileInfo.objects.filter(uzytkownik=request.user)
    for i in info:
        wzrost_cm = i.wzrost
    if ProfileInfo.objects.filter(uzytkownik=request.user).exists():
        if isinstance(wzrost_cm, int):
            wzrost = wzrost_cm/100
            waga = lista_pomiarow[0].wartosc
            bmi = waga / (wzrost*wzrost)
            bmi = round(bmi, 1)
            bmi = str(bmi).replace('.', ',')
            wiadomosc = "Twoje BMI wynosi: " + bmi
            args = {'lista': lista_pomiarow, "wiadomosc": wiadomosc}
    else:
        wiadomosc = 'Nie możemy policzyć Twojego BMI, ponieważ nie podałeś swojego wzrostu. Możesz go dodać poprzez edycję profilu.'
        args = {'lista': lista_pomiarow, "wiadomosc": wiadomosc}
    return render(request, 'pomiary_waga.html', args)

def about(request):
    return render(request, 'about.html')


def nowy_pomiar(request):
    if request.method == 'POST':
        form = PomiarCisnieniaForm(request.POST)
        if form.is_valid():

            skurczowe = form.cleaned_data['skurczowe']
            if skurczowe > 0:
                Pomiar(
                    wartosc=skurczowe,
                    kod_pomiaru='cissku',
                    data_pomiaru=form.cleaned_data['data_cis'],
                    uzytkownik=request.user).save()

            rozkurczowe = form.cleaned_data['rozkurczowe']
            if rozkurczowe > 0:
                Pomiar(
                    wartosc=rozkurczowe,
                    kod_pomiaru='cisroz',
                    data_pomiaru=form.cleaned_data['data_cis'],
                    uzytkownik=request.user).save()

            puls = form.cleaned_data['puls']
            if puls > 0:
                Pomiar(
                    wartosc=puls,
                    kod_pomiaru='puls',
                    data_pomiaru=form.cleaned_data['data_puls'],
                    uzytkownik=request.user).save()

            cukier = form.cleaned_data['cukier']
            if cukier > 0:
                Pomiar(
                    wartosc=cukier,
                    kod_pomiaru='cukier',
                    data_pomiaru=form.cleaned_data['data_cukier'],
                    uzytkownik=request.user).save()

            waga = form.cleaned_data['waga']
            if waga > 0:
                Pomiar(
                    wartosc=waga,
                    kod_pomiaru='waga',
                    data_pomiaru=form.cleaned_data['data_waga'],
                    uzytkownik=request.user).save()

            return redirect('/pomiary')
    else:
        form = PomiarCisnieniaForm()
        args = {'form': form}
        return render(request, 'pomiary_nowy.html', args)


def profile_edit(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST)

        if form.is_valid():
            user = User.objects.get(id=request.user.id)
            user.email = form.cleaned_data['email']
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.save()

            profileinfo = ProfileInfo.objects.get(uzytkownik_id=request.user.id)
            profileinfo.wzrost = form.cleaned_data['wzrost']
            profileinfo.wiek = form.cleaned_data['wiek']
            profileinfo.save()

            return redirect('/profile')
    else:
        user = User.objects.get(id=request.user.id)
        profileinfo = ProfileInfo.objects.get(uzytkownik_id=request.user.id)

        form = EditProfileForm(initial={
                            'first_name': user.first_name,
                            'last_name': user.last_name,
                            'email': user.email,
                            'wiek': profileinfo.wiek,
                            'wzrost': profileinfo.wzrost})

        args = {'form': form}
        return render(request, 'profile_edit.html', args)


def change_password(request):
    if request.method == "POST":
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('zmiana_hasla.html')
        else:
            return redirect('/')
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form}
        return render(request, 'zmiana_hasla.html', args)
